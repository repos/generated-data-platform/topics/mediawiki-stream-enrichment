# MediaWiki Stream Enrichment

The purpose of this repo is to make it easier for us to review a proposed schema for the enriched streams.

We assume the existing schema, stored here, roughly covers our needs. In this repo a modified version will be added and diffed against this to more easily view/define what we want.

https://gerrit.wikimedia.org/r/plugins/gitiles/schemas/event/primary/+/refs/heads/master/jsonschema/mediawiki/revision/create/1.1.0.yaml
